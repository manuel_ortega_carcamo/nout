<?php
use \Firebase\JWT\JWT;
//use \Slim\Middleware\HttpBasicAuthentication;


require 'vendor/autoload.php';

// VER ERRORES ///////////////////////////////////////////////////////////
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
/////////////////////////////////////////////////////////////////////////

//Obtiene los valores ambientales mediante el uso de la librería Dotenv
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();


$config = [
    'settings' => [
        'displayErrorDetails' => true,

        'logger' => [
            'name' => 'slim-app',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/../logs/app.log',
        ],
    ],
];

// MEJORAR LOS ERRORES DEVUELTOS ////////////////////////////////////////
$c = new \Slim\Container($config);
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(500)
                             ->withHeader('Content-Type', 'text/html')
                             ->write('Something went wrong!');
    };
};
/////////////////////////////////////////////////////////////////////////

$app = new Slim\App($config);




//Definiendo las variables de la autenticación con JWT
$app->add(new \Slim\Middleware\JwtAuthentication([
     "path"     => "/",
     "algorithm" => ["HS256"],
     "secure" => false,
    //"secure" => true, //Indico que usaré HTTPS, en la segunda línea indico las excepciones a esto
    "relaxed" => ["localhost","login.dgca.io/api2/"], //Indico que servidor no cumplirá con la regla anterior.
    "secret" => getenv("JWT_SECRET"), //La palabra secreta que enviaré para autenticar mi token recibido.
    //"secret"   => "1dfg2gsd34gsdf56fg7bhrc534gdf4gvc35fdg4fg5",
    
    //"rules"  Contiene Rutas que no se Autetican
    "rules"    => [
          new \Slim\Middleware\JwtAuthentication\RequestPathRule([
               "path"        => "/",
               "passthrough" => ["/hello", "/sesion/crear"]
               ]),
        new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
            "passthrough" => ["OPTIONS"]
        ])
     ],
    //   "callback" => function ($options) use ($app) {
    //     //$app["jwt"] = $arguments["decoded"];
    //     $app->jwt = $options["decoded"];
    // },
    "error" => function ($request, $response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
       // return $response->write("Error: No fue posible ingresar.");
         return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));

$app->add(new \Tuupola\Middleware\Cors([
    "origin" => ["*"],
    "secure" => false,
    "methods" => ["GET","OPTIONS", "POST", "PUT", "PATCH", "DELETE"],
    "headers.allow" => ["Authorization", "X-Requested-With", "content-type","accept"],
    "headers.expose" => ["Etag"],
    "credentials" => true,
    "cache" => 86400
]));

// $app->add(function ($request, $response, $next) {
//     $response['Access-Control-Allow-Origin'] = 'http://yourdomainhere';
//     $response['Access-Control-Allow-Methods'] = 'GET, OPTIONS';
//     $response['Access-Control-Allow-Headers'] = 'X-PINGARUNER';
//     $response['Access-Control-Max-Age'] = 1728000;

//     $response['Content-Type'] = 'application/json';
    
//     $response = $next($request, $response);
    
//     return $response;
// });


//CONFIGURACION NEGOCIO
/******************************************************************/
include ("binn/templates/AccesoLogin.php");
include ("binn/templates/conf.parametro.php");
include ("binn/templates/conf.menu.php");

include ("binn/templates/cursosinscripcion.php");
include ("binn/templates/WebInsc.php");

include ("binn/templates/actividades.php");
include ("binn/templates/cursos.php");

//ROUTES
/******************************************************************/
$app->get('/', function ($request, $response) {
	$body = $response->getBody();
	$body->write(json_encode(array('estado'=>true,'mensaje'=>'API Sistema Club con autenticación','version'=>1.2)));
	return $response->withHeader('Content-Type','application/json')->withBody($body);
});

/**********************************************************************
LOGIN
*********************************************************************/
$oLogin= new clsAccesoLoginVW();

$app->post('/sesion/crear', function ($request, $response, $args) use ($app, $oLogin) {
    $allPostPutVars = $request->getParsedBody();
    $form_data = $allPostPutVars;
    $e = $form_data['email'];
    $p = $form_data['password'];
    $p = sha1($p); //La password debe estar codificada en sha1, de lo contrario, comentar esta línea
    
    $DataUser  = array();
    $DataMenu  = array();
    $DataAccesos  = array();
    //$DataPar  = array();

    $DataUser = $oLogin->vwLogin($form_data['email'],$form_data['password']);
    $tamaño = sizeof($DataUser);
    
    if (sizeof($DataUser) != 0)
    {
      foreach ($DataUser as $Rows):  $IdUsuario = $Rows['idUser']; endforeach;  
      $DataMenu = $oLogin->vwAccesosMenu($IdUsuario);
      $DataAccesos = $oLogin->vwAccesosLogs($IdUsuario,5);
      $key = getenv("JWT_SECRET");
      $jwt = JWT::encode($DataUser, $key);
      $response->withHeader('Content-type', 'application/json');
      
      return  json_encode(array("token" => $jwt,"usuario" => $DataUser,"Menu" => $DataMenu,"UltAccesos" => $DataAccesos));   
    }else{
            return $response->withStatus(400)
                         ->withHeader('Content-Type', 'application/json')
                         ->write(json_encode(array('estado'=>false,'mensaje'=>'Error de autenticacion')));
    }
});



//____________________________________________________________________________
//PARAMETROS
$oPar = new clsParametroVW();

$app->group('/parametros', function () use($app, $oPar){

    $this->get('/',            function ($req, $res, $args) use($app, $oPar){ $body = $res->getBody(); $body->write(json_encode($oPar->vwLista(0))); return $res->withHeader('Content-Type','application/json;charset=utf-8'); });
    $this->get('',             function ($req, $res, $args) use($app, $oPar){ $body = $res->getBody(); $body->write(json_encode($oPar->vwLista(0))); return $res->withHeader('Content-Type','application/json;charset=utf-8'); });
    $this->get('/{id:[0-9]+}', function ($req, $res, $args) use($app, $oPar){ $body = $res->getBody(); $body->write(json_encode($oPar->vwLista($args['id'])) ); return $res->withHeader('Content-Type','application/json;charset=utf-8'); });
//Insert
   // $this->post('/',   function ($req, $res, $args) use($app, $oPar){ $body = $req->getParsedBody(); return $oPar->vwInsFunc($body); });
#Update
    //$this->put('/',    function ($req, $res, $args) use($app, $oPar){ $body = $req->getParsedBody(); return $oPar->vwUpdFunc($body); });
#Delete
    //$this->delete('/', function ($req, $res, $args) use($app, $oPar){ $body = $req->getParsedBody(); return $oPar->vwElimina($body); });


});

//____________________________________________________________________________
//MENU USUARIO
$oMen = new clsMenuVW();

$app->group('/menu', function () use($app, $oMen){

    $this->get('/',  function ($req, $res, $args) use($app, $oMen){ $body = $res->getBody(); $body->write(json_encode($oMen->vwLista(0,0))); return $res->withHeader('Content-Type','application/json;charset=utf-8'); });
    $this->get('',   function ($req, $res, $args) use($app, $oMen){ $body = $res->getBody(); $body->write(json_encode($oMen->vwLista(0,0))); return $res->withHeader('Content-Type','application/json;charset=utf-8'); });
   
});


//____________________________________________________________________________
//FUNCIONARIOS


//____________________________________________________________________________
//CURSOS INSCRIPCION
$oInsCur = new clsCursosInscripcionVW();

$app->group('/cursos/inscripcion', function () use($app, $oInsCur){

    $this->get('/',             function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwLista(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('',              function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwLista(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('/{id:[0-9]+}',  function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwLista($args['id'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
#Insert
    $this->post('/', function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwIns($body); });
    $this->post('',  function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwIns($body); });
#Update
    $this->put('/', function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwUpd($body); });
    $this->put('',  function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwUpd($body); });
#Delete
    $this->delete('/',  function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwElimina($body); });
    $this->delete('',   function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwElimina($body); });
    $this->post('/del', function ($req, $res, $args) use($app, $oInsCur){ $body = $req->getParsedBody(); return $oInsCur->vwElimina($body); });


});

$app->group('/cursos/estadistica', function () use($app, $oInsCur){

    $this->get('/',             function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwListaEstadistica())); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('',              function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwListaEstadistica())); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });


});


$app->group('/invierno', function () use($app, $oInsCur){

    $this->get('/',             function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwListaWEBBACKO(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('',              function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwListaWEBBACKO(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('/{id:[0-9]+}',  function ($req, $res, $args) use($app, $oInsCur){ $body = $res->getBody(); $body->write(json_encode($oInsCur->vwListaWEBBACKO($args['id'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });

});




//____________________________________________________________________________
//PROCESO DE INSCRIPCIPON WEB 

$oWenIns = new clsWebInscVW();



$app->group('/Inscripcion', function () use($app, $oWenIns){

#INSERT ETAPA 1
    $this->post('/', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa1($body); });
    $this->post('',  function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa1($body); });

#INSERT ETAPA 2
    $this->post('/etapados', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa2($body); });

#INSERT ETAPA 3
    $this->get( '/etapatres/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa3Lista($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->post('/etapatres', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa3($body); });

#INSERT ETAPA 4
    $this->get( '/etapacuatro/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa4Lista($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->post('/etapacuatro', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa4($body); });
    $this->post('/etapacuatro/del', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa4Elimina($body); });



#INSERT ETAPA 5
    $this->post('/etapacinco', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa5($body); });



#INSERT ETAPA 6 -FINALIZACION Y ENVIO DE MAIL A EJECUTIVAS

    //IMPRESION DE LA FICHA 
    $this->get( '/etapaseisA/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa6A($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get( '/etapaseisB/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa6B($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get( '/etapaseisC/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa6C($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get( '/etapaseisD/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa6D($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get( '/etapaseisE/{rutnino}',  function ($req, $res, $args) use($app, $oWenIns){ $body = $res->getBody(); $body->write(json_encode($oWenIns->vwEtapa6E($args['rutnino'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    
 
    //ENVIO DE EMAIL
   // $this->post('/etapaseis', function ($req, $res, $args) use($app, $oWenIns){ $body = $req->getParsedBody(); return $oWenIns->vwEtapa6($body); });


});




//____________________________________________________________________________
//ACTIVIDADES
$oAct = new clsActividadesVW();

$app->group('/actividades', function () use($app, $oAct){

    $this->get('/',             function ($req, $res, $args) use($app, $oAct){ $body = $res->getBody(); $body->write(json_encode($oAct->vwLista(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('',              function ($req, $res, $args) use($app, $oAct){ $body = $res->getBody(); $body->write(json_encode($oAct->vwLista(0))); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });
    $this->get('/{id:[0-9]+}',  function ($req, $res, $args) use($app, $oAct){ $body = $res->getBody(); $body->write(json_encode($oAct->vwLista($args['id'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });

});



//____________________________________________________________________________
//ACTIVIDADES
$oCur = new clsCursosVW();

$app->group('/cursos', function () use($app, $oCur){

    $this->get('/{id:[0-9]+}',  function ($req, $res, $args) use($app, $oCur){ $body = $res->getBody(); $body->write(json_encode($oCur->vwLista($args['id'])) ); return $res->withHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8'); });

});






/******************************************************************/
$app->run();
/******************************************************************/
?>
