﻿<?php
/****** GCA_PARametros ************/
include_once ("conf.sql.php"); //Clase con la Configuracion de la Conexion a MySql


class clsCursosInscripcion
{
//  VARIABLES DE CLASE ###################################################
//  CONSTRUCTOR        ###################################################
function __construct() { }
//  METODOS            ###################################################



///////////////////////////////////////////////////////////////////////////


  //RESUMEN INSCRIPCIONES

    public static function ListaWEBBACKO($id)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_WEBBACKO_SelResumen('" .$id ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }
    
///////////////////////////////////////////////////////////////////////////


    //SELECT id=0 (TODOS)
    public static function Lista($id)
    {
    	$obj_Menu=new sQuery();
    	$Sql = "CALL SP_CURSOSINSCRIP_Sel('" .$id ."');";

    	$obj_Menu->executeQuery($Sql);
    	$Datos = $obj_Menu->fetchAll();
    	return $Datos;	
    }

    // //INSERT 
    public static function Ins($r)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_CURSOSINSCRIP_Ins(
                                     '" .$r['IdCurso'] ."',
                                     '" .$r['IdPersona'] ."',
                                     '" .$r['aCancelar'] ."',
                                     '" .$r['IdEmpleado'] ."',
                                     '" .$r['Ano'] ."',
                                     '" .$r['Mes'] ."',
                                     '" .$r['Obs'] ."'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }



    // // - UPDATE 
    public static function Upd($r)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_CURSOSINSCRIP_UpdEstado(
            '" .$r['IdInscrip'] ."',
            '" .$r['Estado'] ."'
            );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }


    //ELIMINACION
    // public static function Elimina($r)
    // {
    //     $obj_Menu=new sQuery();
    //     $Sql = "CALL SP_PROVEE_Eli('" .$r['IdReg'] ."',
    //                                  '" .$r['Estado'] ."'
    //                                  );";

    //     $obj_Menu->executeQuery($Sql);
    //     $Datos = $obj_Menu->fetchAll();
    //     return $Datos;  
    // }



//ESTADISTICA
    public static function ListaEstadistica()
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_CURSOSINSCRIP_SelEstadistica();";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

}

?>
