﻿<?php
/****** GCA_PARametros ************/
include_once ("conf.sql.php"); //Clase con la Configuracion de la Conexion a MySql


class clsFuncionario
{
//  VARIABLES DE CLASE ###################################################
//  CONSTRUCTOR        ###################################################
function __construct() { }
//  METODOS            ###################################################


    //SELECT id=0 (TODOS)
    public static function Lista($Id)
    {
    	$obj_Menu=new sQuery();
    	$Sql = "CALL SP_EMPLEADO_Sel('" .$Id ."');";

    	$obj_Menu->executeQuery($Sql);
    	$Datos = $obj_Menu->fetchAll();
    	return $Datos;	
    }

    //INSERT - UPDATE 
    public static function InsUpd($Id, $Rut, $Dv, $Nombres, $ApePat, $ApeMat, $Email, $Acceso, $User, $Pass, $IdEmpModif)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_EMPLEADO_Ins('" .$Id ."',
                                     '" .$Rut ."',
                                     '" .$Dv ."',
                                     '" .$Nombres ."',
                                     '" .$ApePat ."',
                                     '" .$ApeMat ."',
                                     '" .$Email ."',
                                     '" .$Acceso ."',
                                     '" .$User ."',
                                     '" .$Pass ."',
                                     '" .$IdEmpModif ."'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }


    //ELIMINACION
    public static function Elimina($Id, $Estado, $IdEmpModif)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_EMPLEADO_Eli('" .$Id ."',
                                     '" .$Estado ."',
                                     '" .$IdEmpModif ."'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }


}

?>
