﻿<?php
/****** GCA_PARametros ************/
include_once ("conf.sql.php"); //Clase con la Configuracion de la Conexion a MySql


class clsWebInsc
{
//  VARIABLES DE CLASE ###################################################
//  CONSTRUCTOR        ###################################################
function __construct() { }
//  METODOS            ###################################################





// ------  ETAPA 1 -------------------------------------------------------


    public static function Etapa1($r)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_WEBINSC_InsEtapa1(
                                     '" .$r['Rut'] ."',
                                     '" .$r['Dv'] ."',
                                     '" .$r['Nombres'] ."',
                                     '" .$r['ApePat'] ."',
                                     '" .$r['ApeMat'] ."',
                                     '" .$r['FonoCasa'] ."',
                                     '" .$r['FecNac'] ."',
                                     '" .$r['Email'] ."',
                                     '" .$r['Responsable'] ."',
                                     '1'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

// ------  ETAPA 2 -------------------------------------------------------

    public static function Etapa2($r)
    {
        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_InsEtapa2(
                                     '" .$r['RutNino'] ."',
                                     '" .$r['RutMadre'] ."',
                                     '" .$r['DvMadre'] ."',
                                     '" .$r['NombresMadre'] ."',
                                     '" .$r['ApePatMadre'] ."',
                                     '" .$r['ApeMatMadre'] ."',
                                     '" .$r['EmailMadre'] ."',
                                     '" .$r['CelularMadre'] ."',
                                     '" .$r['FonoEmpresaMadre'] ."',
                                     '" .$r['RutPadre'] ."',
                                     '" .$r['DvPadre'] ."',
                                     '" .$r['NombresPadre'] ."',
                                     '" .$r['ApePatPadre'] ."',
                                     '" .$r['ApeMatPadre'] ."',
                                     '" .$r['EmailPadre'] ."',
                                     '" .$r['CelularPadre'] ."',
                                     '" .$r['FonoEmpresaPadre'] ."'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }




// ------  ETAPA 3 -------------------------------------------------------

    public static function Etapa3($r)
    {
        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_InsEtapa3(
                                     '" .$r['RutNino'] ."',
                                     '" .$r['RutRetira'] ."',
                                     '" .$r['DvRetira'] ."',
                                     '" .strtoupper($r['NombresRetira']) ."',
                                     '1'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    public static function Etapa3Lista($rutnino)
    {
        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_SelEtapa3('" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }




// ------  ETAPA 4 -------------------------------------------------------

    public static function Etapa4($r)
    {

        // $fecha = date("d-m-Y"); // fecha actual 
        // $ano = date("Y"); // Año actual 
        // $mes = date("m"); // Mes actual 
        // $dia = date("d"); // Dia actual 


        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_InsEtapa4(
                                     '" .$r['RutNino'] ."',
                                     '" .$r['IdCurso'] ."',
                                     '0',
                                     '" .date("Y") ."',
                                     '" .date("m") ."',
                                     '1'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    public static function Etapa4Lista($rutnino)
    {
        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_SelEtapa4('" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    public static function Etapa4Elimina($r)
    {
        $obj_Menu=new sQuery();

        $Sql = "CALL SP_WEBINSC_DelEtapa4('" .$r['IdInscrip'] ."','" .$r['Estado'] ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }




// ------  ETAPA 5 -------------------------------------------------------

    public static function Etapa5($r)
    {
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_InsEtapa5(
                                     '" .$r['RutNino'] ."',
                                     '" .$r['TieneSeguro'] ."',
                                     '" .$r['NomPrestador'] ."',
                                     '" .$r['ServicioUrgencia'] ."',
                                     '" .$r['Medicamento'] ."',
                                     '" .$r['Alergias'] ."',
                                     '" .$r['DietaEspecial'] ."',
                                     '" .$r['Peso'] ."',
                                     '" .$r['Obs'] ."',
                                     '" .$r['ComoseEntero'] ."',
                                     '" .$r['AceptoTermino'] ."',
                                     '1'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }




// ------  ETAPA 6 FINALIZAR  -------------------------------------------

    //INFORMACION DEL NINO
    public static function Etapa6A($rutnino)
    {
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_SelEtapa6A( '" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    //INFORMACION DE LOS PADRES
        public static function Etapa6B($rutnino)
    {
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_SelEtapa6B( '" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    //AUTORIZADOS PARA RETIRAR
    public static function Etapa6C($rutnino)
    {  
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_SelEtapa6C( '" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    // CICLOS CONTRATADOS
    public static function Etapa6D($rutnino)
    {
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_SelEtapa6D( '" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

    // INFORMACION RELEVANTE
    public static function Etapa6E($rutnino)
    { 
        $obj_Menu=new sQuery();
        
        $Sql = "CALL SP_WEBINSC_SelEtapa6E( '" .$rutnino ."');";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }





}

?>
