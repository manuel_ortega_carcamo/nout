<?php
/****** GCA_PARametros ************/
include_once ("conf.sql.php"); //Clase con la Configuracion de la Conexion a MySql


class clsParametro
{
//  VARIABLES DE CLASE ###################################################
//  CONSTRUCTOR        ###################################################
function __construct() { }
//  METODOS            ###################################################


    //SELECT id=0 (TODOS)
    public static function Lista($Id)
    {
    	$obj_Menu=new sQuery();
    	$Sql = "CALL SP_PARAM_Descrip1('" .$Id ."');";

    	$obj_Menu->executeQuery($Sql);
    	$Datos = $obj_Menu->fetchAll();
    	return $Datos;	
    }

    //INSERT - UPDATE 
    public static function InsUpd($Id, $Codigo, $Descrip1, $Descrip2, $Obs, $Orden)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_PARAM_Ins('" .$Id ."',
                                     '" .$Codigo ."',
                                     '" .$Descrip1 ."',
                                     '" .$Descrip2 ."',
                                     '" .$Obs ."',
                                     '" .$Orden ."'
                                     );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }
   


    //ELIMINACION
    public static function Elimina($Id, $Estado, $IdEmpModif)
    {
        $obj_Menu=new sQuery();
        $Sql = "CALL SP_PARAM_Eli('" .$Id ."',
                                  '" .$Estado ."',
                                  '" .$IdEmpModif ."'
                                 );";

        $obj_Menu->executeQuery($Sql);
        $Datos = $obj_Menu->fetchAll();
        return $Datos;  
    }

}

?>
