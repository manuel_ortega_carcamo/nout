<?Php

include_once ("binn/inc/clsPersonas.php");
include_once ("binn/inc/clsEnvioMail.php");

// include_once ("../inc/clsPersonas.php");
// include_once ("../inc/clsEnvioMail.php");
class clsPersonasVW
{



    function __construct() { }  //  CONSTRUCTOR   

    //  METODOS 

    //SELECT id=0 (TODOS)
    public static function vwLista($Rut)
    {
        $Clase=new clsPersonas();
        $Datos = $Clase->Lista($tipo,$id);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            // $respuesta[$i]['IdReg'] =   $Rows['PRO_Id'];
            $respuesta[$i]['Rut'] =   $Rows['SOC_Rut'];
            $respuesta[$i]['Dv'] =   $Rows['SOC_Dv'];
            $respuesta[$i]['Nombres'] =  utf8_encode($Rows['SOC_Nombres']);
            $respuesta[$i]['ApePat'] =  utf8_encode($Rows['SOC_ApePat']);
            $respuesta[$i]['ApeMat'] =  utf8_encode($Rows['SOC_ApeMat']);
            $respuesta[$i]['FonoCasa'] =  utf8_encode($Rows['SOC_FonoCasa']);
            $respuesta[$i]['FonoEmpresa'] =  utf8_encode($Rows['SOC_FonoEmpresa']);
            $respuesta[$i]['Celular'] =  utf8_encode($Rows['SOC_Celular']);
            $respuesta[$i]['FecNac'] =  utf8_encode($Rows['SOC_FecNac']);
            $respuesta[$i]['Email'] =  utf8_encode($Rows['SOC_Email']);
            $respuesta[$i]['ModificadoPor'] =  utf8_encode($Rows['ModificadoPor']);
            $respuesta[$i]['Responsable'] =  utf8_encode($Rows['SOC_Responsable']);
            $respuesta[$i]['FechaIngreso'] =  utf8_encode($Rows['SOC_FechaIngreso']);
            $respuesta[$i]['Estado'] =   $Rows['SOC_Estado'];

         $i++;
        endforeach;  
        return $respuesta;	
    }


    //INSERT - UPDATE 
    public static function vwIns($r)
    {

        $e['NomDestinatario']  =$r['Nombres'] .' ' .$r['ApePat'] .' ' .$r['ApeMat'];
        $e['MailDestinatario'] =$r['Email']; 
        
        if($e['MailDestinatario']  == ""){ 
            $e['NomDestinatario']  ="NO llega el correo";
            $e['MailDestinatario'] ="mortega@providencia.cl";
        }
        
        $Clase=new clsPersonas();
        $Datos = $Clase->Ins($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta['Id'] =   $Rows['id'];
            $respuesta['Msg'] =  utf8_encode($Rows['msg']);
            $respuesta['Hash'] = $Rows['txthash'];

         $i++;
        endforeach;  

        //ENVIO DE EMAIL DE CONFIRMACION
        $e['Asunto'] = 'CONFIRMA TU CORREO, Para continuar';
        $e['Template'] = '/home/club/api/mail/templates/contentsClub.php';
        $e['link'] ="http://club.dgca.io/api/registro/?op=" .$respuesta['Hash'];
        $oMail=new clsEnvioMail();
        $DatosMail = $oMail->EnvioMailGmail($e);


        return $respuesta;  
    }
    //UPDATE 
    public static function vwUpd($r)
    {
        $Clase=new clsPersonas();
        $Datos = $Clase->Upd($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta[$i]['Id'] =   $Rows['id'];
            $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
            $respuesta[$i]['Hash'] = $Rows['txthash'];
         $i++;
        endforeach;  

        return $respuesta;  
    }

//ELIMINACION
// public static function vwElimina($r)
// {
//     $Clase=new clsPersonas();
//     $Datos = $Clase->Elimina($r);
//     $respuesta  = array();

//     $i=0;
//     foreach ($Datos as $Rows):
//         $respuesta[$i]['Id'] =   $Rows['id'];
//         $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
//      $i++;
//     endforeach;  
//     return $respuesta;  
// }

/**********************************************************************************************
FIN CLASE 
***********************************************************************************************/
}


//$oClase = new clsPersonasVW();



// $r['Rut']='66666666'; 
// $r['Dv']='6'; 
// $r['Nombres']='NICOLE VIVIANNA';  
// $r['ApePat']='GUAGUITA'; 
// $r['ApeMat']='veloz'; 
// $r['FonoCasa']='123'; 
// $r['FonoEmpresa']='456'; 
// $r['Celular']='789'; 
// $r['FecNac']='1987-08-28'; 
// $r['Email']='guaguita@gmail.com'; 
// $r['Responsable']='Manuel Ortega Carcamo'; 
// $r['EMP_IdIngresa']='1';


//     $Clase=new clsPersonas();
//     $Datos = $Clase->Ins($r);
//     $respuesta  = array();

//     $i=0;
//     foreach ($Datos as $Rows):
//         $respuesta['Id'] =   $Rows['id'];
//         $respuesta['Msg'] =  utf8_encode($Rows['msg']);
//         $respuesta['Hash'] = $Rows['txthash'];
//         //$r['hash'] = $Rows['txthash'];
        
//      $i++;
//     endforeach;  
    

    // echo json_encode($respuesta);

    // $r['Asunto'] = 'LA GUAGUTA MAS RUDA';
    // $r['Template'] = '/home/club/api/mail/templates/contentsClub.php';
    // $r['NomDestinatario'] ='MANULAO ORTEGAO';
    // $r['MailDestinatario'] ='mortega@providencia.cl';
    // $r['link'] ="http://club.dgca.io/api/registro/?op=" .$respuesta['Hash'];
    
    // $oMail=new clsEnvioMail();
    // $DatosMail = $oMail->EnvioMailGmail($r);


//echo json_encode($oClase->vwLista(0));


//INSERTA
//$r['Id']='0'; 
// $r['Rut']='66666666'; 
// $r['Dv']='6'; 
// $r['Nombres']='NICOLE VIVIANNA';  
// $r['ApePat']='GUAGUITA'; 
// $r['ApeMat']='veloz'; 
// $r['FonoCasa']='123'; 
// $r['FonoEmpresa']='456'; 
// $r['Celular']='789'; 
// $r['FecNac']='1987-08-28'; 
// $r['Email']='guaguita@gmail.com'; 
// $r['Responsable']='Manuel Ortega Carcamo'; 
// $r['EMP_IdIngresa']='1';
// echo json_encode($oClase->vwIns($r));

//ACTUALIZA
// $r['Id']='3'; 
// $r['Rut']='33333333'; 
// $r['Dv']='3'; 
// $r['Nombres']='GUAGUITA VELOZ';  
// $r['ApePat']='GUAGUITA'; 
// $r['ApeMat']='veloz'; 
// $r['FonoCasa']='123'; 
// $r['FonoEmpresa']='456'; 
// $r['Celular']='789'; 
// $r['FecNac']='1987-08-28'; 
// $r['Email']='guaguita@gmail.com'; 
// $r['Responsable']='Manuel Ortega Carcamo'; 
// $r['EMP_IdIngresa']='1'; 
// echo json_encode($oClase->vwUpd($r));



// $r['IdReg']='20'; 
// $r['Estado']='2'; 
// echo json_encode($oClase->vwElimina($r));



?>
