<?Php

include_once ("binn/inc/clsAccesoLogin.php");
//include_once ("../inc/clsAccesoLogin.php");
class clsAccesoLoginVW
{



function __construct() { }  //  CONSTRUCTOR   

//  METODOS 

    public static function vwLogin($email,$password)
    {
        $Clase=new clsAccesoLogin();
        $Datos = $Clase->Login($email,$password);
        $respuesta  = array();

        $i=0;

        foreach ($Datos as $Rows):

            $respuesta[$i]['idUser'] =   $Rows['EMP_Id'];
            $respuesta[$i]['nombreFullUser'] =  utf8_encode($Rows['NombreFull']);
            $respuesta[$i]['nombreUser'] =  utf8_encode($Rows['EMP_Nombres']);
            $respuesta[$i]['nombreApePatUser'] =   utf8_encode($Rows['EMP_ApePat']);
            $respuesta[$i]['nombreApeMatUser'] =   utf8_encode($Rows['EMP_ApeMat']);
            $respuesta[$i]['imagen'] = utf8_encode($Rows['EMP_Img']);
            $respuesta[$i]['Email'] = utf8_encode($Rows['EMP_Email']);
            $respuesta[$i]['User'] =   $Rows['EMP_User'];
            $respuesta[$i]['Dashboard'] =   $Rows['EAC_Dashboard'];
            
            $respuesta[$i]['exp'] = time() + (6000);
          
         $i++;
        endforeach;  
        //$jsondata["funcionarios"]= $respuesta;
        //return json_encode($respuesta);
        return $respuesta; ///$jsondata;	
    }



    public static function vwAccesosMenu($IdFuncionario)
    {
        $Clase=new clsAccesoLogin();
        $Datos = $Clase->AccesosMenu($IdFuncionario);
        $respuesta  = array();
        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['Perfil'] =   $Rows['PEE_Nombre'];
            $respuesta[$i]['idMenu'] =  $Rows['MEN_Id'];
            $respuesta[$i]['nombreMenu'] =  utf8_encode($Rows['MEN_Nombre']);
            $respuesta[$i]['padreMenu'] =   $Rows['MEN_Padre'];
            $respuesta[$i]['linkMenu'] =   utf8_encode($Rows['MEN_Url']);
            $respuesta[$i]['imagenMenu'] = utf8_encode($Rows['MEN_Img']);
            $respuesta[$i]['separadorMenu'] = $Rows['MEN_ConSeparador'];
            $respuesta[$i]['ordenMenu'] =   $Rows['MEN_Orden'];
         $i++;
        endforeach;  
        return $respuesta; ///$jsondata;    
    }

    public static function vwAccesosLogs($IdFuncionario,$LimitReg)
    {
        $Clase=new clsAccesoLogin();
        $Datos = $Clase->vwAccesosLogs($IdFuncionario,$LimitReg);
        $respuesta  = array();
        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['Fecha'] =   $Rows['Fecha'];
            $respuesta[$i]['Hora'] =  $Rows['Hora'];
         $i++;
        endforeach;  
        return $respuesta; ///$jsondata;    
    }




/**********************************************************************************************
  GESTION DE ACCESO A LA APLICACION
***********************************************************************************************/
}


//$oClase = new clsAccesoLoginVW();
//echo json_encode($oClase->vwLogin("mortega","mortega"));

//echo json_encode(array("menu"=>$oClase->vwAccesosMenu(4)));


?>
