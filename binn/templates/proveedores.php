<?Php

include_once ("binn/inc/clsProveedores.php");
//include_once ("../inc/clsProveedores.php");
class clsProveedoresVW
{



function __construct() { }  //  CONSTRUCTOR   

//  METODOS 

//SELECT id=0 (TODOS)
public static function vwLista($tipo,$id)
{
    $Clase=new clsProveedores();
    $Datos = $Clase->Lista($tipo,$id);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):

        $respuesta[$i]['IdReg'] =   $Rows['PRO_Id'];
        $respuesta[$i]['Rut'] =   $Rows['PRO_Rut'];
        $respuesta[$i]['Dv'] =   $Rows['PRO_Dv'];
        $respuesta[$i]['RazonSocial'] =  utf8_encode($Rows['PRO_RazonSocial']);
        $respuesta[$i]['Giro'] =  utf8_encode($Rows['PRO_Giro']);
        $respuesta[$i]['Direccion'] =  utf8_encode($Rows['PRO_Direccion']);
        $respuesta[$i]['IdComuna'] =   $Rows['PRO_IdComuna'];
        $respuesta[$i]['Comuna'] =  utf8_encode($Rows['Comuna']);
        $respuesta[$i]['Estado'] =   $Rows['PRO_Estado'];

     $i++;
    endforeach;  
    return $respuesta;	
}


//INSERT - UPDATE 
public static function vwIns($r)
{
    $Clase=new clsProveedores();
    $Datos = $Clase->Ins($r);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}
//UPDATE 
public static function vwUpd($r)
{
    $Clase=new clsProveedores();
    $Datos = $Clase->Ins($r);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}
//ELIMINACION
public static function vwElimina($r)
{
    $Clase=new clsProveedores();
    $Datos = $Clase->Elimina($r);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}

/**********************************************************************************************
  LISTADO DE USUARIO
***********************************************************************************************/
}


// $oClase = new clsProveedoresVW();

//echo json_encode($oClase->vwLista(0));


//INSERTA
// $r['IdReg']='0'; 
// $r['Rut']='22222222'; 
// $r['Dv']='2'; 
// $r['RazonSocial']='poraqui por alla';  
// $r['Giro']='Venta y compra de cosas'; 
// $r['Direccion']='av Por aqui esquina por alla'; 
// $r['IdComuna']='86'; 
// echo json_encode($oClase->vwIns($r));

//ACTUALIZA
// $r['IdReg']='5'; 
// $r['Rut']='55555555'; 
// $r['Dv']='5'; 
// $r['RazonSocial']='555poraqui por alla';  
// $r['Giro']='555Venta y compra de cosas'; 
// $r['Direccion']='555av Por aqui esquina por alla'; 
// $r['IdComuna']='90'; 
// echo json_encode($oClase->vwUpd($r));



// $r['IdReg']='20'; 
// $r['Estado']='2'; 
// echo json_encode($oClase->vwElimina($r));



?>
