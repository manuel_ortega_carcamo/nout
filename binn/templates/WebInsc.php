<?Php

include_once ("binn/inc/clsWebInsc.php");
include_once ("binn/inc/clsEnvioMail.php");


// include_once ("../inc/clsWebInsc.php");
// include_once ("../inc/clsEnvioMail.php");

class clsWebInscVW
{



    function __construct() { }  //  CONSTRUCTOR   



// ------  ETAPA 1 -------------------------------------------------------

    public static function vwEtapa1($r)
    {
        

        //  --- DATOS DEL EMAIL A ENVIAR ---
        $e['NomDestinatario']  =$r['Nombres'] .' ' .$r['ApePat'] .' ' .$r['ApeMat'];
        $e['MailDestinatario'] =$r['Email']; 
        
        if($e['MailDestinatario']  == ""){ 
            $e['NomDestinatario']  ="NO llego el correo";
            $e['MailDestinatario'] ="mortega@providencia.cl";
        }


        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa1($r);
        $respuesta  = array();

        $i=0;
                foreach ($Datos as $Rows):
            $respuesta['Id'] =   $Rows['id'];
            $respuesta['Msg'] =  utf8_encode($Rows['msg']);
            $respuesta['Hash'] = $Rows['txthash'];

         $i++;
        endforeach;   



        //ENVIO DE EMAIL DE CONFIRMACION
        $e['Asunto'] = 'CONFIRMA TU CORREO, Para continuar';
        $e['Template'] = '/home/club/api/mail/templates/contentsClub.php';
        $e['link'] ="http://club.dgca.io/api/registro/?op=" .$respuesta['Hash'];
        $oMail=new clsEnvioMail();
        $DatosMail = $oMail->EnvioMailGmail($e);


        return $respuesta;  
    }
  
    

// ------  ETAPA 2 -------------------------------------------------------

 public static function vwEtapa2($r)
    {
        $m['NomDestinatario']  =$r['NombresMadre'] .' ' .$r['ApePatMadre'] .' ' .$r['ApeMatMadre'];
        $m['MailDestinatario'] =$r['EmailMadre']; 

        $p['NomDestinatario']  =$r['NombresPadre'] .' ' .$r['ApePatPadre'] .' ' .$r['ApeMatPadre'];
        $p['MailDestinatario'] =$r['EmailPadre']; 


        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa2($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta['IdMadre'] =   $Rows['IdMadre'];
            $respuesta['txtHashMadre'] =   $Rows['txtHashMadre'];
            $respuesta['IdPadre'] =   $Rows['IdPadre'];
            $respuesta['txtHashPadre'] =   $Rows['txtHashPadre'];
         $i++;
        endforeach;   

        
         //ENVIO DE EMAIL DE CONFIRMACION
        //  MADRE
      
        $m['Asunto'] = 'CONFIRMA TU CORREO, Para continuar';
        $m['Template'] = '/home/club/api/mail/templates/contentsClub.php';
        $m['link'] ="http://club.dgca.io/api/registro/?op=" .$respuesta['txtHashMadre'];
        $oMail=new clsEnvioMail();
        $DatosMail = $oMail->EnvioMailGmail($m);

        
         //ENVIO DE EMAIL DE CONFIRMACION
        //  PADRE
        $p['Asunto'] = 'CONFIRMA TU CORREO, Para continuar';
        $p['Template'] = '/home/club/api/mail/templates/contentsClub.php';
        $p['link'] ="http://club.dgca.io/api/registro/?op=" .$respuesta['txtHashPadre'];
        $oMail=new clsEnvioMail();
        $DatosMail = $oMail->EnvioMailGmail($p);



        return $respuesta;  
    }
  



// ------  ETAPA 3 -------------------------------------------------------

 public static function vwEtapa3($r)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa3($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta['id'] =   $Rows['id'];
            $respuesta['msg'] =   $Rows['msg'];
         $i++;
        endforeach;   

        return $respuesta;  
    }

 public static function vwEtapa3Lista($rut)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa3Lista($rut);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['IdRet'] =   $Rows['RET_Id'];
            $respuesta[$i]['RutCompleto'] =   $Rows['RET_Rut']  .' ' .$Rows['RET_Dv'];
            $respuesta[$i]['Nombres'] =  utf8_encode($Rows['RET_Nombres']);

         $i++;
        endforeach;   

       return $respuesta;  
        // return json_encode($r);
    }






// ------  ETAPA 4 -------------------------------------------------------

 public static function vwEtapa4($r)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa4($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta['id'] =   $Rows['id'];
            $respuesta['msg'] =   $Rows['msg'];
         $i++;
        endforeach;   

        return $respuesta;  
    }



 public static function vwEtapa4Lista($rut)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa4Lista($rut);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['IdInscrip'] =   $Rows['IdInscrip'];
            $respuesta[$i]['IdCurso'] =   $Rows['IdCurso'];
            $respuesta[$i]['IdAct'] =   $Rows['IdAct'];
            $respuesta[$i]['IdSocio'] =   $Rows['SOC_I'];
            $respuesta[$i]['Actividad'] =   utf8_encode($Rows['ACT_nombre']);
            $respuesta[$i]['Curso'] =   utf8_encode($Rows['CUA_Nombre']);
            $respuesta[$i]['Valor'] =   $Rows['CUA_Valor'];

         $i++;
        endforeach;   

       return $respuesta;  
        // return json_encode($r);
    }

 public static function vwEtapa4Elimina($r)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa4Elimina($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['Id'] = $Rows['id'];
            $respuesta[$i]['msg'] = $Rows['msg'];
         $i++;
        endforeach;   

       return $respuesta;  
        // return json_encode($r);
    }


// ------  ETAPA 5 -------------------------------------------------------

 public static function vwEtapa5($r)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa5($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta['id'] =   $Rows['id'];
            $respuesta['msg'] =   $Rows['msg'];
         $i++;
        endforeach;   

        return $respuesta;  
    }


// ------  ETAPA 6 -------------------------------------------------------

 public static function vwEtapa6($RutNinoINC)
    {

         //  --- DATOS DEL EMAIL A ENVIAR ---

        
        

        $Clase=new clsWebInsc();
        $DatosNiNo = $Clase->Etapa6A($RutNinoINC);
        $respnino  = array();

        $i=0;
        foreach ($DatosNiNo as $Rows):
            $respnino['rutnino'] =   $Rows['SOC_Rut'];
            $respnino['rutninodv'] =   $Rows['SOC_Dv'];
            $respnino['nombrenino'] =   $Rows['Nino'];
            $respnino['fecnacnino'] =   $Rows['SOC_FecNac'];
            $respnino['edadnino'] =   $Rows['Edad'];
            $respnino['fononino'] =   $Rows['SOC_FonoCasa'];
            $respnino['contactonino'] =   $Rows['SOC_Responsable'];
            $respnino['emailnino'] =   $Rows['SOC_Email'];
        endforeach;   



        $Datos = $Clase->Etapa6B($RutNinoINC);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta[$i]['rut'] =   $Rows['Rut'];
            $respuesta[$i]['nombres'] =   $Rows['Nombres'];
            $respuesta[$i]['email'] =   $Rows['SOC_Email'];
            $respuesta[$i]['fono'] =   $Rows['SOC_FonoEmpresa'];
            $respuesta[$i]['movil'] =   $Rows['SOC_Celular'];
         $i++;
        endforeach;   


        $DatosRetira = $Clase->Etapa6C($RutNinoINC);
        $Html="";
        foreach ($DatosRetira as $Rec):

            $Html= $Html .$Rec['Rut'] ." | " .utf8_encode($Rec['RET_Nombres']) ."<br />";
            

            // $respRetira[$i]['rut'] =   $R['Rut'];
            // $respRetira[$i]['nombres'] =   $R['RET_Nombres'];
            
         $i++;
        endforeach;




        $DatosCiclos = $Clase->Etapa6D($RutNinoINC);
        $HtmlCiclos="";
        foreach ($DatosCiclos as $Rciclo):
            $HtmlCiclos= $HtmlCiclos .$Rciclo['Servicio'] ." | " .$Rciclo['CUI_FecInsc'] ."<br />";
        $i++;
        endforeach;  

        


        $e['NomDestinatario']  ='INSCRIPCION WEB';
        $e['MailDestinatario'] ='registroweb@clubprovidencia.cl'; 
        //$e['MailDestinatario'] ='mortega@clubprovidencia.cl'; 

        //ENVIO DE EMAIL DE CONFIRMACION
        $e['Asunto'] = 'INSCRIPCION WEB:' .$respnino['nombrenino']  ;
        $e['Template'] = '/home/club/api/mail/templates/contentsNewIncripClub.php';
     

        $e['rutninodv'] = $respnino['rutnino'] ."-" .$respnino['rutninodv'] ;
        $e['rutninoincrip'] = $respnino['rutnino'] ;
        $e['nombrenino'] = $respnino['nombrenino'] ;
        $e['fecnacnino'] = $respnino['fecnacnino'] ;
        $e['edadnino'] = $respnino['edadnino'] ;
        $e['fononino'] = $respnino['fononino'] ;
        $e['contactonino'] = $respnino['contactonino'] ;
        $e['emailnino'] = $respnino['emailnino'] ;


        $e['rutmadre'] = $respuesta[0]['rut'] ;
        $e['nombresmadre'] = $respuesta[0]['nombres'];
        $e['emailmadre'] = $respuesta[0]['email'];
        $e['movilmadre'] = $respuesta[0]['movil'];
        $e['fonomadre'] = $respuesta[0]['fono'];



        $e['rutpadre'] = $respuesta[1]['rut'] ;
        $e['nombrespadre'] = $respuesta[1]['nombres'];
        $e['emailpadre'] = $respuesta[1]['email'];
        $e['movilpadre'] = $respuesta[1]['movil'];
        $e['fonopadre'] = $respuesta[1]['fono'];


        $e['tablaautorizados'] = $Html;
        $e['tablaciclos'] = $HtmlCiclos;





        $oMail=new clsEnvioMail();
        $DatosMail = $oMail->EnvioNewInscripcion($e);




        //return $respuesta;  
        return $e;
    }








// ------  ETAPA 6 PRINT -------------------------------------------------------


 public static function vwEtapa6A($rutnino)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa6A($rutnino);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['IdInscrip'] =   $Rows['WIE_Id'];
            $respuesta[$i]['IdNino'] =   $Rows['SOC_Id'];
            $respuesta[$i]['rutcompleto'] =   $Rows['rutcompleto'];

            
            $respuesta[$i]['Rut'] =   $Rows['SOC_Rut'];
            $respuesta[$i]['Dv'] =   $Rows['SOC_Dv'];
            $respuesta[$i]['Nino'] =   utf8_encode($Rows['Nino']);
            $respuesta[$i]['Nombres'] =   utf8_encode($Rows['SOC_Nombres']);
            $respuesta[$i]['ApePat'] =   utf8_encode($Rows['SOC_ApePat']);
            $respuesta[$i]['ApeMat'] =   utf8_encode($Rows['SOC_ApeMat']);
            $respuesta[$i]['FecNac'] =   utf8_encode($Rows['SOC_FecNac']);
            $respuesta[$i]['Edad'] =   $Rows['Edad'];
            $respuesta[$i]['FechaInsc'] =   utf8_encode($Rows['FechaInsc']);
            $respuesta[$i]['FonoCasa'] =   utf8_encode($Rows['SOC_FonoCasa']);
            $respuesta[$i]['Responsable'] =   utf8_encode($Rows['SOC_Responsable']);
            $respuesta[$i]['Email'] =   utf8_encode($Rows['SOC_Email']);

         $i++;
        endforeach;   
       return $respuesta;  
    }



 public static function vwEtapa6B($rutnino)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa6B($rutnino);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):

            $respuesta[$i]['IdInscrip'] =   $Rows['WIE_Id'];
            $respuesta[$i]['IdNino'] =   $Rows['SOC_Id'];
            $respuesta[$i]['Rut'] =   utf8_encode($Rows['Rut']);

            $respuesta[$i]['Detalle'] =   utf8_encode($Rows['Detalle']);
            $respuesta[$i]['Nombres'] =   utf8_encode($Rows['Nombres']);
            $respuesta[$i]['Email'] =   utf8_encode($Rows['SOC_Email']);
            $respuesta[$i]['FonoEmpresa'] =   utf8_encode($Rows['SOC_FonoEmpresa']);
            $respuesta[$i]['Celular'] =   utf8_encode($Rows['SOC_Celular']);

         $i++;
        endforeach;   
       return $respuesta;  
    }    

 public static function vwEtapa6C($rutnino)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa6C($rutnino);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta[$i]['Rut'] =   $Rows['Rut'];
            $respuesta[$i]['Nombres'] =   utf8_encode($Rows['RET_Nombres']);
         $i++;
        endforeach;   
       return $respuesta;  
    }  

 public static function vwEtapa6D($r)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa6D($r);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta[$i]['Idnscrip'] =   $Rows['Idnscrip'];
            $respuesta[$i]['Servicio'] =   utf8_encode($Rows['Servicio']);
            $respuesta[$i]['Monto'] =   $Rows['Monto'];
            $respuesta[$i]['Actividad'] =   utf8_encode($Rows['Actividad']);
            $respuesta[$i]['Curso'] =   utf8_encode($Rows['Curso']);
            $respuesta[$i]['FecInsc'] =   utf8_encode($Rows['CUI_FecInsc']);
         $i++;
        endforeach;   
       return $respuesta;  
    }  

 public static function vwEtapa6E($rutnino)
    {
        $Clase=new clsWebInsc();
        $Datos = $Clase->Etapa6E($rutnino);
        $respuesta  = array();

        $i=0;
        foreach ($Datos as $Rows):
            $respuesta[$i]['IdRelevante'] =   $Rows['INR_Id'];
            $respuesta[$i]['IdNino'] =   $Rows['SOC_Id'];
            $respuesta[$i]['TieneSeguro'] =   $Rows['PAR_TieneSeguro'];
            $respuesta[$i]['NomPrestador'] =   utf8_encode($Rows['NomPrestador']);

            $respuesta[$i]['ServicioUrgencia'] =   utf8_encode($Rows['ServicioUrgencia']);
            $respuesta[$i]['Medicamento'] =   utf8_encode($Rows['Medicamento']);
            $respuesta[$i]['Alergias'] =   utf8_encode($Rows['Alergias']);
            $respuesta[$i]['DietaEspecial'] =   utf8_encode($Rows['DietaEspecial']);
            $respuesta[$i]['Peso'] =   $Rows['INR_Peso'];
            $respuesta[$i]['Obs'] =   utf8_encode($Rows['Obs']);
            $respuesta[$i]['IdComoseEntero'] =   $Rows['PAR_ComoseEntero'];
            $respuesta[$i]['FechaIngreso'] =   utf8_encode($Rows['INR_FechaIngreso']);
            $respuesta[$i]['IdAceptoTermino'] =  $Rows['PAR_AceptoTermino'];
            $respuesta[$i]['IdEstado'] =   $Rows['INR_Estado'];

         $i++;
        endforeach;   
       return $respuesta;  
    } 


/**********************************************************************************************
FIN CLASE 
***********************************************************************************************/
}


//$oClase = new clsWebInscVW();

// $r['IdInscrip']  ="111";
// $r['Estado']  ="2";
// echo json_encode($oClase->vwEtapa4Elimina($r));




// $r['Rut']='77789'; 
// $r['Dv']='6'; 
// $r['Nombres']='NICOLE VIVIANNA';  
// $r['ApePat']='GUAGUITA'; 
// $r['ApeMat']='veloz'; 
// $r['FonoCasa']='123'; 
// $r['FecNac']='1987-08-28'; 
// $r['Email']='mortega@apgca.cl'; 
// $r['Responsable']='Manuel Ortega Carcamo'; 

// echo json_encode($oClase->vwEtapa1($r));


//ETAPA 2

// $r['RutNino'] = '77789';
// $r['RutMadre'] = '33422';
// $r['DvMadre'] = '5';
// $r['NombresMadre'] = 'AURELIA';
// $r['ApePatMadre'] = 'CARCAMO';
// $r['ApeMatMadre'] = 'SEPULVEDA';
// $r['EmailMadre'] = 'MORTEGA@PROVIDENCIA.CL';
// $r['CelularMadre'] = '9999999';
// $r['FonoEmpresaMadre'] = '88888888';
// $r['RutPadre'] = '54556';
// $r['DvPadre'] = '6';
// $r['NombresPadre'] = 'ALEJANDRO';
// $r['ApePatPadre'] = 'SALAZAR';
// $r['ApeMatPadre'] = 'ORTIZ';
// $r['EmailPadre'] = 'MORTEGA@APGCA.CL';
// $r['CelularPadre'] = '33333333';
// $r['FonoEmpresaPadre'] = '44444444';
// echo json_encode($oClase->vwEtapa2($r));

//PRINT_R(json_encode($r));


// //ETAPA 3
// $r['RutNino']='77789';
// $r['RutRetira']='111333';
// $r['DvRetira']='4';
// $r['NombresRetira']='SOFIA RETIRRONA';
// $r['IdEmpleado']='1';
// echo json_encode($oClase->vwEtapa3($r));

// LISTAR RETIRAN

// $r['rutnino']='23274412';
// echo json_encode($oClase->vwEtapa3Lista($r));



// //ETAPA 4
// $r['RutNino']='99999999';
// $r['IdCurso']='2';
// echo json_encode($oClase->vwEtapa4($r));


//ETAPA 5

// $r['RutNino'] = '77789';
// $r['TieneSeguro'] = '1';
// $r['NomPrestador'] = 'BANMEDICA';
// $r['ServicioUrgencia'] = 'HELP';
// $r['Medicamento'] = 'DIPIRONA, ASPIRINA';
// $r['Alergias'] = 'ZANCUDOS';
// $r['DietaEspecial'] = 'ARROS CON PURE';
// $r['Peso'] = '87';
// $r['Obs'] = 'INSCRIPCION WEB';
// $r['ComoseEntero'] = '4';
// $r['AceptoTermino'] = '1';
// $r['IdEmpleado']  = '1';
// echo json_encode($oClase->vwEtapa5($r));



?>
