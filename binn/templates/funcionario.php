<?Php

include_once ("binn/inc/clsFuncionario.php");
//include_once ("../inc/clsFuncionario.php");
class clsFuncionarioVW
{



function __construct() { }  //  CONSTRUCTOR   

//  METODOS 

//SELECT id=0 (TODOS)
public static function vwLista($id)
{
    $Clase=new clsFuncionario();
    $Datos = $Clase->Lista($id);
    $respuesta  = array();
    //$jsondata = array();

    $i=0;

    // foreach ($Datos as $Rows):
    //     $json = array(
    //                     [$i]['id']=> $Rows['EMP_Id'],
    //                     [$i]['nombres'] => utf8_encode($Rows['EMP_Nombres']),
    //                     [$i]['apepat']  => utf8_encode($Rows['EMP_ApePat'])
    //                 );
    //     i++;
    // endforeach; 



    foreach ($Datos as $Rows):

        $respuesta[$i]['idreg'] =   $Rows['EMP_Id'];
        $respuesta[$i]['rut'] =   $Rows['EMP_Rut'];
        $respuesta[$i]['dv'] =   $Rows['EMP_Dv'];
        $respuesta[$i]['nombres'] =  utf8_encode($Rows['EMP_Nombres']);
        $respuesta[$i]['apepat'] =   utf8_encode($Rows['EMP_ApePat']);
        $respuesta[$i]['apemat'] =   utf8_encode($Rows['EMP_ApeMat']);
        $respuesta[$i]['email'] =    utf8_encode($Rows['EMP_Email']);
        $respuesta[$i]['idacceso'] =   $Rows['EMP_Acceso'];
        $respuesta[$i]['acceso'] =   $Rows['Acceso'];
        $respuesta[$i]['User'] =   $Rows['EMP_User'];
        $respuesta[$i]['Pass'] =   $Rows['EMP_Pass'];
        $respuesta[$i]['idEstado'] =   $Rows['EMP_Estado'];
        $respuesta[$i]['estado'] =   $Rows['Estado'];
        $respuesta[$i]['fechamodifica'] =   $Rows['EMP_FechaModif'];
        $respuesta[$i]['modificadopor'] =   utf8_encode($Rows['ModificadoPor']);
      
     $i++;
    endforeach;  
    //$jsondata["funcionarios"]= $respuesta;
    //return json_encode($respuesta);
    return $respuesta; ///$jsondata;	
}


//INSERT - UPDATE 
public static function vwInsFunc($r)
{
    $Clase=new clsFuncionario();
    $Datos = $Clase->InsUpd(0,
                            $r['Rut'], 
                            $r['Dv'], 
                            $r['Nombres'], 
                            $r['ApePat'], 
                            $r['ApeMat'], 
                            $r['Email'], 
                            $r['Acceso'], 
                            $r['User'], 
                            $r['Pass'], 
                            $r['IdEmpModif']);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}
//UPDATE 
public static function vwUpdFunc($r)
{
    $Clase=new clsFuncionario();
    $Datos = $Clase->InsUpd($r['Id'],
                            $r['Rut'], 
                            $r['Dv'], 
                            $r['Nombres'], 
                            $r['ApePat'], 
                            $r['ApeMat'], 
                            $r['Email'], 
                            $r['Acceso'], 
                            $r['User'], 
                            $r['Pass'], 
                            $r['IdEmpModif']);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}
//ELIMINACION
public static function vwElimina($r)
{
    $Clase=new clsFuncionario();
    $Datos = $Clase->Elimina($r['Id'], $r['Estado'], $r['IdEmpModif']);
    $respuesta  = array();

    $i=0;
    foreach ($Datos as $Rows):
        $respuesta[$i]['Id'] =   $Rows['id'];
        $respuesta[$i]['Msg'] =  utf8_encode($Rows['msg']);
     $i++;
    endforeach;  
    return $respuesta;  
}

/**********************************************************************************************
  LISTADO DE USUARIO
***********************************************************************************************/
}

/*
$oClase = new clsFuncionarioVW();
echo json_encode($oClase->vwLista(0));

//echo json_encode($oClase->vwInsUpdFunc(0, 22222222, '2', 'TOMAS', 'FRANCO', 'ORTEGA', 'TOMACHO@BEBE.cl', 0, null, null, 1));
echo json_encode($oClase->vwElimina(11, 2, 1));
*/

?>
